import React from 'react';
import logo from './img/logo.svg';
import './style/App.scss';
import './style/normalize.scss';
import Home from './components/Home/Home'

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Home/>
      </header>
    </div>
  );
}

export default App;
