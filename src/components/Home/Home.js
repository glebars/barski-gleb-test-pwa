import React from 'react';
import icon from '../../img/shop-icon.png'
import InstallationModal from '../InstallationModal/InstallationModal'
import useIsIOS from '../customHooks/useIsIOS' 
import JsonLdHelmet from '../JsonLdHelmet/JsonLdHelmet'
const getPushNotification = () => {
  Notification.requestPermission(function(status) {
    console.log('Notification permission status:', status);
    if (Notification.permission === 'granted') {
      navigator.serviceWorker.getRegistration().then(function(reg) {
        var options = {
          body: 'Click to get to the b0arding app',
          icon: icon,
          vibrate: [100, 50, 100],
          data: {
            dateOfArrival: Date.now(),
            primaryKey: 1
          }
          // actions: [
          //   {action: 'explore', title: 'Explore this new world',
          //     icon: 'images/checkmark.png'},
          //   {action: 'close', title: 'Close notification',
          //     icon: 'images/xmark.png'},
          // ]
        };
        reg.showNotification('Simple push notification', options)
      });
    }
  })
}

const Home =() => {
  const {prompt} = useIsIOS()

  return (
    <div className='home-container'> 
      <p className='home-container__text'>It is absolutely simple PWA for testing purposes. Here I am testing some features of progressive web applicatiobn in order to gain some experience and test my knowldge.</p>
      <p className='home-container__text'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        <h4 className="App-link">Install this app</h4>
        <h4 onClick={getPushNotification} className="push-notifications_btn">Get a push notification</h4>
        {prompt && <InstallationModal/>}
        <JsonLdHelmet/>
    </div>
  );
}

export default Home;
