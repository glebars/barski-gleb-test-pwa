import React, { useState } from "react";
import image from "../../img/shop-icon.png";
import share from "../../img/share.png";
import plus4 from "../../img/4.png";

const InstallPWA = ({...props}) => {
    const [openedModal, setOpenedModal] = useState(true);

  return (
    <div className='i-modal'>
      {openedModal && <div className='i-modal__wrapper'>
        {/* <div className='i-modal__wrapper'> */}
          <div onClick={() => setOpenedModal(false)} className="modal__cross modal__cross_i-modal"></div>
          <img
          src={image}
          className="i-modal__image"
          height="72"
          width="72"
          alt="Install PWA"
          />
            <h3 className='i-modal__header'>Install this great Web App</h3>
            <p className='i-modal__text'>Install this application on your homescreen for a better experience.</p>
            <div className='i-modal__actios-wrapper'>
              <div>
                Tap "Share"
                <img
                src={share}
                className='i-modal__icon'
                alt="Share icon"
                />
              </div>
              <div>
                choose "Add to Home Screen"
                <img
                className='i-modal__icon'
                src={plus4}
                alt="Add to homescreen icon"
                />
              </div>
            </div>
        {/* </div> */}
      </div>
      }
  </div>
  )
}

export default InstallPWA