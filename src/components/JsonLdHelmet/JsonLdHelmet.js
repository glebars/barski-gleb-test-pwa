import React from 'react';
import Helmet from "react-helmet";

const JsonLdHelmet = () => {
  return (
    <Helmet
      script={[
        {
          type: "application/ld+json",
          innerHTML: `
          {
            "@context": "https://schema.org",
            "@type": "Website",
            "name": "gleb Barskiy test PWA",
            "legalName": "gleb Barskiy test PWA",
            "author": "Barskiy Gleb",
            "url": "https://gleb-barskiy-test-pwa.netlify.app/",
            "logo": "https://www.flaticon.com/free-icon/shop_181648",
            "datePublished": "2021-10-18",
            "description": "It is absolutely simple PWA for testing purposes",
            "contactPoint": {
              "@type": "ContactPoint",
              "contactType": "customer support",
              "email": "glebars2004@gmail.com"
            },
            "aggregateRating": {
              "@type": "AggregateRating",
              "ratingValue": "4.8",
              "reviewCount": "9",
              "bestRating": "5",
              "worstRating": "1"
            }
          }`
        }
      ]}
    />
  );
};

export default JsonLdHelmet;
